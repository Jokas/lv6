﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp7
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        double op, num;
        int count;

        private void button10_Click(object sender, EventArgs e)
        {
            text1.Text = text1.Text + 7;
        }

        private void nula_Click(object sender, EventArgs e)
        {
            text1.Text = text1.Text + 0;
        }

        private void tocka_Click(object sender, EventArgs e)
        {
            text1.Text = text1.Text + ',' ;
        }

        private void jedan_Click(object sender, EventArgs e)
        {
            text1.Text = text1.Text + 1;
        }

        private void dva_Click(object sender, EventArgs e)
        {
            text1.Text = text1.Text + 2;
        }

        private void tri_Click(object sender, EventArgs e)
        {
            text1.Text = text1.Text + 3;
        }

        private void cetiri_Click(object sender, EventArgs e)
        {
            text1.Text = text1.Text + 4;
        }

        private void pet_Click(object sender, EventArgs e)
        {
            text1.Text = text1.Text + 5;
        }

        private void sest_Click(object sender, EventArgs e)
        {
            text1.Text = text1.Text + 6;
        }

        private void osam_Click(object sender, EventArgs e)
        {
            text1.Text = text1.Text + 8;
        }

        private void devet_Click(object sender, EventArgs e)
        {
            text1.Text = text1.Text + 9;
        }

        private void plus_Click(object sender, EventArgs e)
        {
            op = double.Parse(text1.Text);
            text1.Clear();
            text1.Focus();
            count = 1;

        }

        private void minus_Click(object sender, EventArgs e)
        {
            op = double.Parse(text1.Text);
            text1.Clear();
            text1.Focus();
            count = 2;
        }

        private void puta_Click(object sender, EventArgs e)
        {
            op = double.Parse(text1.Text);
            text1.Clear();
            text1.Focus();
            count = 3;
        }

        private void podj_Click(object sender, EventArgs e)
        {
            op = double.Parse(text1.Text);
            text1.Clear();
            text1.Focus();
            count = 4;
        }

        private void sqrt_Click(object sender, EventArgs e)
        {
            double isqrt = Double.Parse(text1.Text);
            isqrt = Math.Sqrt(isqrt);
            text1.Text = isqrt.ToString();
        }

        private void sin_Click(object sender, EventArgs e)
        {
            double isin = Double.Parse(text1.Text);
            isin = Math.Sin(isin);
            text1.Text = isin.ToString();
        }

        private void cos_Click(object sender, EventArgs e)
        {
            double icos = Double.Parse(text1.Text);
            icos = Math.Cos(icos);
            text1.Text = icos.ToString();
        }

        private void tan_Click(object sender, EventArgs e)
        {
            double itan = Double.Parse(text1.Text);
            itan = Math.Tan(itan);
            text1.Text = itan.ToString();
        }

        private void log_Click(object sender, EventArgs e)
        {
            double ilog = Double.Parse(text1.Text);
            ilog = Math.Log10(ilog);
            text1.Text = ilog.ToString();
        }

        private void jednako_Click(object sender, EventArgs e)
        {
            calculate();
        }

        private void clean_Click(object sender, EventArgs e)
        {
            text1.Clear();
        }

        private void text1_TextChanged(object sender, EventArgs e)
        {
            
        }

        public void calculate()
        {
            switch (count)
            {
                case 1:
                    num = op + double.Parse(text1.Text);
                    text1.Text = num.ToString();
                    break;
                case 2:
                    num = op - double.Parse(text1.Text);
                    text1.Text = num.ToString();
                    break;
                case 3:
                    num = op * double.Parse(text1.Text);
                    text1.Text = num.ToString();
                    break;
                case 4:
                    num = op / double.Parse(text1.Text);
                    text1.Text = num.ToString();
                    break;
                default:
                    break;
            }


        }
    }
}
